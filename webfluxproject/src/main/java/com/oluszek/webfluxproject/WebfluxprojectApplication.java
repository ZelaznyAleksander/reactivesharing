package com.oluszek.webfluxproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebfluxprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebfluxprojectApplication.class, args);
	}

}
