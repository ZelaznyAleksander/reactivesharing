package com.oluszek.webfluxproject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Random;

@RestController
public class TestController {

    Random random = new Random();

    private static WebClient webClient = WebClient
            .builder()
            .baseUrl("http://localhost:8081/test")
            .build();

    private static WebClient webClient2 = WebClient
            .builder()
            .baseUrl("http://localhost:8082/test")
            .build();

    private static WebClient webClient3 = WebClient
            .builder()
            .baseUrl("http://localhost:8083/test")
            .build();

    private static WebClient webClient4 = WebClient
            .builder()
            .baseUrl("http://localhost:8084/test")
            .build();

    WebClient[] clients = {webClient, webClient2, webClient3, webClient4};

    @GetMapping("/action")
    public Mono<String> getApiData() {
        Object a = new Object();
        Object b = new Object();
        Object c = new Object();
        Object d = new Object();
        Object e = new Object();
        Object f = new Object();
        Object g = new Object();
        return clients[random.nextInt(4)].get().retrieve().bodyToMono(String.class);
    }
}
