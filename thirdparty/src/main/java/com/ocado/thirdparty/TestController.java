package com.ocado.thirdparty;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.time.Duration;

@RestController
public class TestController {

    @GetMapping("/test")
    public Mono<String> getApiData() throws InterruptedException {
        return Mono.just("OK")
                .delayElement(Duration.ofMillis(300));

    }
}
