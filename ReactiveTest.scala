import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class ReactiveTest extends Simulation {

val baseUrl = "http://localhost:8080"
val httpProtocol = http.baseUrl(baseUrl);

val scn1 = scenario("Scenario 1").exec(
    repeat(30) {
    exec(http("Get Hello")
        .get("/action")
        .check(status.is(200))
    ).pause(1 second)
    })


setUp(scn1.inject(rampUsers(1000) during (30 seconds)).protocols(httpProtocol))

}

// 300  500 700 900 1100    1300    1500    1700    2000    2300    2600    2800    3000    3400    3600    3800    4000    4300    4600